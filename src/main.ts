import { createApp } from 'vue'
import App from './App.vue'

// Import css
import "@/assets/css/main.css";

createApp(App).mount('#app')